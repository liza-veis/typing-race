import { Room } from '../../types';

export class RoomRepository {
  private rooms = new Map<string, Room>();

  private startedRooms = new Set<string>();

  getRooms(): Room[] {
    return [...this.rooms.values()];
  }

  getRoom(roomName: string): Room | null {
    return this.rooms.get(roomName) || null;
  }

  getUserRoom(username: string): Room | null {
    const rooms = this.getRooms();

    return rooms.find(({ users }) => users.some(({ name }) => name === username)) || null;
  }

  createRoom(roomName: string): Room {
    const room = {
      name: roomName,
      usersConnected: 0,
      users: [],
      canJoin: true,
    };

    this.setRoom(roomName, room);

    return room;
  }

  setRoom(roomName: string, room: Room): void {
    this.rooms.set(roomName, room);
  }

  deleteRoom(roomName: string): void {
    this.rooms.delete(roomName);
  }

  isRoomExist(roomName: string): boolean {
    return this.rooms.has(roomName);
  }

  isGameStarted(roomName: string): boolean {
    return this.startedRooms.has(roomName);
  }

  startGame(roomName: string): void {
    this.startedRooms.add(roomName);
  }

  finishGame(roomName: string): void {
    this.startedRooms.delete(roomName);
  }
}
