import { Server } from 'socket.io';
import { selectUsername } from './helpers/socket.helper';
import { checkUsername } from './helpers/user.helper';
import { GameService } from './services/game.service';
import { RoomService } from './services/room.service';
import { UserService } from './services/user.service';

export default (io: Server): void => {
  io.on('connection', (socket) => {
    const username = selectUsername(socket);

    const userService = new UserService(io, socket);
    const gameService = new GameService(io, socket);
    const roomService = new RoomService(io, socket);

    const isUsernameExists = checkUsername(io, socket);

    if (isUsernameExists) {
      userService.handleUsernameExists();
      return;
    }

    roomService.updateUserRooms();

    const onCreateRoom = (roomName: string) => {
      roomService.onCreateRoom(roomName);
    };

    const onJoinRoom = (roomName: string) => {
      const user = userService.createUser(username);
      roomService.onJoinRoom(roomName, user);
    };

    const onLeaveRoom = (roomName: string) => {
      roomService.onLeaveRoom(roomName, username);
      gameService.removeUserFromResults(username);
    };

    const onSetReadyStatus = (roomName: string) => {
      roomService.onSetReadyStatus(roomName, username);
    };

    const onSetProgress = (roomName: string, progress: number) => {
      roomService.onSetProgress(roomName, username, progress);

      if (progress === 100) {
        gameService.addUserToResults(username);
      }
    };

    const onStartGame = (roomName: string) => {
      const isGameStarted = roomService.isGameStarted(roomName);
      if (isGameStarted) return;

      roomService.startGame(roomName);
      gameService.startGame(roomName);
      gameService.sendTextId(roomName);
    };

    const onFinishGame = (roomName: string) => {
      const isGameStarted = roomService.isGameStarted(roomName);
      if (!isGameStarted) return;

      const room = roomService.getRoom(roomName);
      if (!room) return;

      roomService.finishGame(roomName);
      gameService.finishGame(room);
    };

    const onDisconnect = () => {
      roomService.onUserDisconnect(username);
    };

    socket.on('CREATE_ROOM', onCreateRoom);
    socket.on('JOIN_ROOM', onJoinRoom);
    socket.on('LEAVE_ROOM', onLeaveRoom);
    socket.on('SET_READY_STATUS', onSetReadyStatus);
    socket.on('SET_PROGRESS', onSetProgress);
    socket.on('START_GAME', onStartGame);
    socket.on('FINISH_GAME', onFinishGame);
    socket.on('disconnect', onDisconnect);
  });
};
