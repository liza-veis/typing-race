export const countdown = (
  startValue: number,
  onUpdate: (value: number) => void,
  onFinish: () => void
): NodeJS.Timeout | null => {
  let value = startValue - 1;

  onUpdate(value);

  if (value <= 0) {
    onFinish();
    return null;
  }

  const interval = setInterval(() => {
    onUpdate(value);
    value -= 1;

    if (value < 0) {
      onFinish();
      clearInterval(interval);
    }
  }, 1000);

  return interval;
};
