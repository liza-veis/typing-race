import { Server, Socket } from 'socket.io';
import { getConnectedSockets, selectUsername } from './socket.helper';
import { User } from '../../types';

export const checkUsername = (io: Server, socket: Socket): boolean => {
  const username = selectUsername(socket);

  if (!username) return false;

  const connectedSockets = getConnectedSockets(io);

  return connectedSockets.some((connectedSocket) => {
    if (connectedSocket.id === socket.id) return false;

    const socketUsername = selectUsername(connectedSocket);
    if (socketUsername !== username) return false;

    return true;
  });
};

export const toggleUserReadyStatus = (users: User[], username: string): User[] => {
  const activeUser = users.find(({ name }) => name === username);

  return users.map((user) => {
    if (user === activeUser) {
      return { ...user, isReady: !activeUser.isReady };
    }
    return user;
  });
};

export const setUserProgress = (users: User[], username: string, progress: number): User[] => {
  const activeUser = users.find(({ name }) => name === username);

  return users.map((user) => {
    if (user === activeUser) {
      return { ...user, progress };
    }
    return user;
  });
};

export const resetUsers = (users: User[]): User[] => {
  return users.map((user) => ({
    ...user,
    isReady: false,
    progress: 0,
  }));
};
