import { Server, Socket } from 'socket.io';

export const selectUsername = (socket: Socket): string =>
  socket.handshake.query['username']?.toString() || '';

export const getConnectedSockets = (io: Server): Socket[] => [...io.sockets.sockets.values()];
