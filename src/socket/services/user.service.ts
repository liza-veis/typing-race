import { Server, Socket } from 'socket.io';
import { User } from '../../types';

export class UserService {
  io: Server;

  socket: Socket;

  constructor(io: Server, socket: Socket) {
    this.io = io;
    this.socket = socket;
  }

  handleUsernameExists(): void {
    this.socket.emit('USERNAME_EXISTS');
  }

  createUser(username: string): User {
    return {
      name: username,
      isReady: false,
      progress: 0,
    };
  }
}
