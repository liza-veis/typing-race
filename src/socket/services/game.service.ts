import { Server, Socket } from 'socket.io';
import data from '../../data';
import { SECONDS_FOR_GAME, SECONDS_TIMER_BEFORE_START_GAME } from '../config';
import { countdown } from '../helpers/game.helper';
import { Room } from '../../types';

export class GameService {
  io: Server;

  socket: Socket;

  results: Set<string> = new Set();

  private timer: NodeJS.Timeout | null = null;

  private gameTime: NodeJS.Timeout | null = null;

  constructor(io: Server, socket: Socket) {
    this.io = io;
    this.socket = socket;
  }

  startGame(roomName: string): void {
    const startValue = SECONDS_TIMER_BEFORE_START_GAME;
    const onUpdate = (value: number) => this.io.to(roomName).emit('UPDATE_TIMER', value);
    const onFinish = () => {
      this.io.to(roomName).emit('FINISH_TIMER');
      this.startGameTime(roomName);
      this.timer = null;
    };

    this.timer = countdown(startValue, onUpdate, onFinish);
  }

  finishGame(room: Room): void {
    this.clearIntervals();

    const { users } = room;
    const restUsers = users
      .filter(({ name }) => !this.results.has(name))
      .sort((a, b) => b.progress - a.progress)
      .map(({ name }) => name);

    const results = Array.from(this.results).concat(restUsers);

    this.io.to(room.name).emit('GAME_RESULTS', results);
  }

  startGameTime(roomName: string): void {
    const startValue = SECONDS_FOR_GAME;
    const onUpdate = (value: number) => this.io.to(roomName).emit('UPDATE_GAME_TIME', value);
    const onFinish = () => {
      this.io.to(roomName).emit('FINISH_GAME_TIME');
      this.gameTime = null;
    };

    this.gameTime = countdown(startValue, onUpdate, onFinish);
  }

  sendTextId(roomName: string): void {
    const textId = Math.floor(Math.random() * data.texts.length);

    this.io.to(roomName).emit('TEXT_ID', textId);
  }

  addUserToResults(username: string): void {
    this.results.add(username);
  }

  removeUserFromResults(username: string): void {
    this.results.delete(username);
  }

  clearIntervals(): void {
    if (this.timer !== null) {
      clearInterval(this.timer);
    }
    if (this.gameTime !== null) {
      clearInterval(this.gameTime);
    }
  }
}
