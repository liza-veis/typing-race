import { Server, Socket } from 'socket.io';
import { roomRepository } from '../repositories';
import { MAXIMUM_USERS_FOR_ONE_ROOM } from '../config';
import { resetUsers, setUserProgress, toggleUserReadyStatus } from '../helpers/user.helper';
import { Room, User } from '../../types';

export class RoomService {
  io: Server;

  socket: Socket;

  store = roomRepository;

  constructor(io: Server, socket: Socket) {
    this.io = io;
    this.socket = socket;
  }

  handleRoomExists(): void {
    this.socket.emit('ROOM_EXISTS');
  }

  onCreateRoom(roomName: string): void {
    const isRoomExist = this.store.isRoomExist(roomName);
    if (isRoomExist) {
      this.handleRoomExists();
      return;
    }

    this.store.createRoom(roomName);

    const rooms = this.store.getRooms();

    this.socket.emit('CREATE_ROOM_DONE', roomName);
    this.io.emit('UPDATE_ROOMS', rooms);
  }

  onJoinRoom(roomName: string, user: User): void {
    const room = this.store.getRoom(roomName);
    if (!room) return;

    this.socket.join(roomName);

    const users = [...room.users, user];
    const usersConnected = users.length;
    const canJoin = this.getCanJoin(roomName, usersConnected);
    const newRoom = { ...room, users, usersConnected, canJoin };

    this.updateRoomOnUsersChange(newRoom);
    this.socket.emit('JOIN_ROOM_DONE', roomName);
  }

  onLeaveRoom(roomName: string, username: string): void {
    const room = this.store.getRoom(roomName);
    if (!room) return;

    this.socket.leave(roomName);

    const users = room.users.filter((user) => user.name !== username);
    const usersConnected = users.length;
    const canJoin = this.getCanJoin(roomName, usersConnected);
    const newRoom = { ...room, users, usersConnected, canJoin };

    if (usersConnected) {
      this.updateRoomOnUsersChange(newRoom);
    } else {
      this.deleteRoom(roomName);
    }
  }

  onSetReadyStatus(roomName: string, username: string): void {
    const room = this.store.getRoom(roomName);
    if (!room) return;

    const users = toggleUserReadyStatus(room.users, username);
    const newRoom = { ...room, users };

    this.store.setRoom(roomName, newRoom);

    this.io.to(roomName).emit('UPDATE_ROOM', newRoom);
  }

  onSetProgress(roomName: string, username: string, progress: number): void {
    const room = this.store.getRoom(roomName);
    if (!room) return;

    const users = setUserProgress(room.users, username, progress);
    const newRoom = { ...room, users };

    this.store.setRoom(roomName, newRoom);

    this.io.to(roomName).emit('UPDATE_ROOM', newRoom);
  }

  onUserDisconnect(username: string): void {
    const room = this.store.getUserRoom(username);
    if (!room) return;

    this.onLeaveRoom(room.name, username);
  }

  getRoom(roomName: string): Room | null {
    return this.store.getRoom(roomName);
  }

  deleteRoom(roomName: string): void {
    this.store.deleteRoom(roomName);

    const rooms = this.store.getRooms();

    this.io.emit('UPDATE_ROOMS', rooms);
  }

  updateUserRooms(): void {
    const rooms = this.store.getRooms();
    this.socket.emit('UPDATE_ROOMS', rooms);
  }

  updateRoomOnUsersChange(newRoom: Room): void {
    const { name } = newRoom;
    this.store.setRoom(name, newRoom);

    const rooms = this.store.getRooms();

    this.io.emit('UPDATE_ROOMS', rooms);
    this.io.to(name).emit('UPDATE_ROOM', newRoom);
  }

  startGame(roomName: string): void {
    this.store.startGame(roomName);

    const room = this.store.getRoom(roomName);
    if (!room) return;

    const newRoom = {
      ...room,
      canJoin: this.getCanJoin(room.name, room.usersConnected),
    };

    this.updateRoomOnUsersChange(newRoom);
  }

  finishGame(roomName: string): void {
    this.store.finishGame(roomName);

    const room = this.store.getRoom(roomName);
    if (!room) return;

    const newRoom = {
      ...room,
      users: resetUsers(room.users),
      canJoin: this.getCanJoin(room.name, room.usersConnected),
    };

    this.updateRoomOnUsersChange(newRoom);
  }

  isGameStarted(roomName: string): boolean {
    return this.store.isGameStarted(roomName);
  }

  getCanJoin(roomName: string, usersConnected: number): boolean {
    const isMaxUsers = usersConnected >= MAXIMUM_USERS_FOR_ONE_ROOM;
    const isGameStarted = this.store.isGameStarted(roomName);

    return !isMaxUsers && !isGameStarted;
  }
}
