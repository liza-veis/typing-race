import { createElement } from '../helpers/dom.helper';

export type ResultItemElementProps = {
  username: string;
  place: number;
};

export const createResultItem = ({ username, place }: ResultItemElementProps): HTMLElement => {
  const resultItemElement = createElement({ tagName: 'li' });

  const usernameElement = createElement({
    tagName: 'span',
    attributes: { id: `place-${place}` },
  });

  usernameElement.textContent = username;
  resultItemElement.textContent = `${place}. `;
  resultItemElement.append(usernameElement);

  return resultItemElement;
};
