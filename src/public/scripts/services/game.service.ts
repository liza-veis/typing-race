import { COLOR_GREEN } from 'scripts/constants';
import { createElement } from 'scripts/helpers/dom.helper';
import { Socket } from 'socket.io-client';

const timerElement = document.querySelector<HTMLElement>('#timer');
const gameTimeElement = document.querySelector<HTMLElement>('.time-left');
const gameWrapper = document.querySelector<HTMLElement>('.game-wrapper');
const textContainer = document.querySelector<HTMLElement>('#text-container');

type GameServiceProps = {
  socket: Socket;
};

export class GameService {
  socket: Socket;

  roomName: string | null = null;

  text = '';

  private isGameStarted = false;

  private nextCharIndex = 0;

  constructor({ socket }: GameServiceProps) {
    this.socket = socket;

    this.onKeyup = this.onKeyup.bind(this);
    this.createCharElement = this.createCharElement.bind(this);
  }

  prestartGame(roomName: string): void {
    this.roomName = roomName;
    this.isGameStarted = true;

    this.resetGame();

    this.socket.emit('START_GAME', this.roomName);
  }

  startGame(): void {
    this.setGame();

    window.addEventListener('keyup', this.onKeyup);
  }

  finishGame(): void {
    this.isGameStarted = false;
    this.resetGame();

    window.removeEventListener('keyup', this.onKeyup);
    this.socket.emit('FINISH_GAME', this.roomName);
  }

  updateTimer(value: number): void {
    if (timerElement) {
      timerElement.textContent = value.toString();
    }
  }

  updateGameTime(value: number): void {
    if (gameTimeElement) {
      gameTimeElement.textContent = value.toString();
    }
  }

  async setText(textId: number): Promise<void> {
    const data = await fetch(`/game/texts/${textId}`);

    this.text = await data.text();
  }

  isStarted(): boolean {
    return this.isGameStarted;
  }

  setGame(): void {
    if (timerElement && gameWrapper) {
      timerElement.style.display = 'none';
      gameWrapper.style.display = 'block';
    }
    if (textContainer) {
      const elements = Array.from(this.text).map(this.createCharElement);

      textContainer.append(...elements);
      this.setCharBorder(this.nextCharIndex);
    }
  }

  resetGame(): void {
    this.text = '';
    this.nextCharIndex = 0;

    if (timerElement && gameWrapper) {
      timerElement.textContent = '';
      timerElement.style.display = 'block';
      gameWrapper.style.display = 'none';
    }
    if (textContainer) {
      textContainer.innerHTML = '';
    }
  }

  private onKeyup(ev: KeyboardEvent) {
    if (ev.key !== this.text[this.nextCharIndex]) return;

    this.setCharBackground(this.nextCharIndex);
    this.setCharBorder(this.nextCharIndex + 1);
    this.nextCharIndex += 1;

    const progress = this.getProgress();

    this.socket.emit('SET_PROGRESS', this.roomName, progress);
  }

  private createCharElement(char: string): HTMLElement {
    const element = createElement({ tagName: 'span' });
    element.textContent = char;

    return element;
  }

  private setCharBorder(charIndex: number): void {
    const element = textContainer?.children[charIndex] as HTMLElement;
    if (element) {
      element.style.borderBottom = `1px solid ${COLOR_GREEN}`;
    }
  }

  private setCharBackground(charIndex: number): void {
    const element = textContainer?.children[charIndex] as HTMLElement;
    if (element) {
      element.style.backgroundColor = COLOR_GREEN;
    }
  }

  private getProgress(): number {
    return Math.min((this.nextCharIndex / this.text.length) * 100, 100);
  }
}
