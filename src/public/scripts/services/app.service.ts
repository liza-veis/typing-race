import { Socket } from 'socket.io-client';
import { RoomService } from './room.service';
import { GameService } from './game.service';
import { createRoom } from 'scripts/components/Room';
import { Room, User } from '../types';
import { createResultItem } from 'scripts/components/ResultItem';

const roomsContainer = document.querySelector<HTMLElement>('.rooms-container');
const createRoomButton = document.querySelector<HTMLElement>('#add-room-btn');
const roomsPageElement = document.querySelector<HTMLElement>('#rooms-page');
const gamePageElement = document.querySelector<HTMLElement>('#game-page');
const leaveRoomButton = document.querySelector<HTMLElement>('#quit-room-btn');
const quitResultsBtn = document.querySelector<HTMLElement>('#quit-results-btn');
const readyButton = document.querySelector<HTMLElement>('#ready-btn');
const gameElement = document.querySelector<HTMLElement>('.game');
const modalContainer = document.querySelector<HTMLElement>('.modal-container');
const resultsElement = document.querySelector<HTMLElement>('.results');

type AppServiceProps = {
  socket: Socket;
  roomService: RoomService;
  gameService: GameService;
  username: string;
};

export class AppService {
  socket: Socket;

  roomService: RoomService;

  gameService: GameService;

  username: string;

  constructor({ socket, roomService, gameService, username }: AppServiceProps) {
    this.roomService = roomService;
    this.gameService = gameService;
    this.socket = socket;
    this.username = username;

    this.joinRoom = this.joinRoom.bind(this);
    this.leaveRoom = this.leaveRoom.bind(this);
    this.createRoom = this.createRoom.bind(this);
    this.setReadyStatus = this.setReadyStatus.bind(this);
    this.onModalClose = this.onModalClose.bind(this);

    createRoomButton?.addEventListener('click', this.createRoom);
    readyButton?.addEventListener('click', this.setReadyStatus);
    leaveRoomButton?.addEventListener('click', this.leaveRoom);
    quitResultsBtn?.addEventListener('click', this.onModalClose);
  }

  createRoom(): void {
    const name = window.prompt('Input room name');

    if (name) {
      this.socket.emit('CREATE_ROOM', name);
    }
  }

  updateRooms(rooms: Room[]): void {
    if (!roomsContainer) return;

    const roomsToCreate = rooms
      .filter(({ canJoin }) => canJoin)
      .map(({ name, usersConnected }) => ({
        name,
        usersConnected,
        onJoinRoom: this.joinRoom,
      }));

    const elements = roomsToCreate.map(createRoom);

    roomsContainer.innerHTML = '';
    roomsContainer.append(...elements);
  }

  setActiveRoom(roomName: string): void {
    if (roomsPageElement && gamePageElement) {
      roomsPageElement.style.display = 'none';
      gamePageElement.style.display = 'flex';
    }

    this.roomService.setRoom(roomName);
  }

  resetActiveRoom(): void {
    if (roomsPageElement && gamePageElement) {
      roomsPageElement.style.display = 'block';
      gamePageElement.style.display = 'none';
    }

    this.roomService.resetRoom();
  }

  showError(message: string, callback?: () => void): void {
    window.alert(message);
    if (callback) callback();
  }

  joinRoom(roomName: string): void {
    this.socket.emit('JOIN_ROOM', roomName);
  }

  leaveRoom(): void {
    const roomName = this.roomService.getRoomName();

    this.socket.emit('LEAVE_ROOM', roomName);
    this.resetActiveRoom();
  }

  updateRoom(room: Room): void {
    const { users } = room;
    const { isReady } = users.find(({ name }) => name === this.username) || {};

    const actualUsers = this.addFlagToActiveUser(users);

    if (readyButton) {
      readyButton.textContent = isReady ? 'Not Ready' : 'Ready';
    }

    this.roomService.updateUsers(actualUsers);
  }

  setReadyStatus(): void {
    const roomName = this.roomService.getRoomName();

    this.socket.emit('SET_READY_STATUS', roomName);
  }

  setGame(): void {
    if (readyButton) {
      readyButton.style.display = 'none';
    }
    if (leaveRoomButton) {
      leaveRoomButton.style.display = 'none';
    }
    if (gameElement) {
      gameElement.style.display = 'block';
    }
  }

  resetGame(): void {
    if (readyButton) {
      readyButton.style.display = 'block';
    }
    if (leaveRoomButton) {
      leaveRoomButton.style.display = 'block';
    }
    if (gameElement) {
      gameElement.style.display = 'none';
    }
  }

  showModal(results: string[]): void {
    if (modalContainer) {
      modalContainer.style.display = 'flex';
    }

    const elements = results.map((username, place) =>
      createResultItem({ username, place: place + 1 })
    );

    resultsElement?.append(...elements);
  }

  onModalClose(): void {
    if (modalContainer) {
      modalContainer.style.display = 'none';
    }
    if (resultsElement) {
      resultsElement.innerHTML = '';
    }

    this.resetGame();
  }

  private addFlagToActiveUser(users: User[]) {
    return users.map((user) => {
      if (user.name === this.username) {
        return { ...user, isActive: true };
      }
      return user;
    });
  }
}
