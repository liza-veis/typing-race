import { Socket } from 'socket.io-client';
import { createUserItem } from 'scripts/components/UserItem';
import { User } from 'scripts/types';

const gameTitle = document.querySelector<HTMLElement>('.game-title');
const usersContainer = document.querySelector<HTMLElement>('.user-list');
const leaveRoomButton = document.querySelector<HTMLElement>('#quit-room-btn');

type RoomServiceProps = {
  socket: Socket;
};

export class RoomService {
  socket: Socket;

  private roomName: string | null = null;

  constructor({ socket }: RoomServiceProps) {
    this.socket = socket;
  }

  getRoomName(): string | null {
    return this.roomName;
  }

  setRoom(roomName: string): void {
    if (leaveRoomButton) {
      leaveRoomButton.style.display = 'block';
    }
    if (gameTitle) {
      gameTitle.textContent = roomName;
    }
    this.roomName = roomName;
  }

  resetRoom(): void {
    if (leaveRoomButton) {
      leaveRoomButton.style.display = 'none';
    }
    if (gameTitle) {
      gameTitle.textContent = '';
    }
    this.roomName = null;
  }

  updateUsers(users: User[]): void {
    if (usersContainer) {
      const elements = users.map(createUserItem);

      usersContainer.innerHTML = '';
      usersContainer.append(...elements);
    }
  }
}
