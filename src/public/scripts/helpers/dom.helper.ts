type CreateElementProps = {
  tagName?: string;
  className?: string;
  attributes?: { [name: string]: string };
};

export const createElement = ({
  tagName,
  className,
  attributes = {},
}: CreateElementProps): HTMLElement => {
  const element = document.createElement(tagName || 'div');

  if (className) {
    const classNames = formatClassNames(className);
    element.classList.add(...classNames);
  }

  Object.entries(attributes).forEach(([key, value]) => element.setAttribute(key, value));

  return element;
};

export const formatClassNames = (className: string): string[] =>
  className.split(' ').filter(Boolean);
