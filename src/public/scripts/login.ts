const username = sessionStorage.getItem('username');

if (username) {
  window.location.replace('/game');
}

const submitButton = document.querySelector<HTMLButtonElement>('#submit-button');
const input = document.querySelector<HTMLInputElement>('#username-input');

const getInputValue = () => input?.value;

const onClickSubmitButton = () => {
  const inputValue = getInputValue();
  if (!inputValue) {
    return;
  }
  sessionStorage.setItem('username', inputValue);
  window.location.replace('/game');
};

const onKeyUp = (ev: KeyboardEvent) => {
  const enterKeyCode = 'Enter';
  if (ev.key === enterKeyCode) {
    submitButton?.click();
  }
};

submitButton?.addEventListener('click', onClickSubmitButton);
window.addEventListener('keyup', onKeyUp);
