import { io } from 'socket.io-client';
import { AppService, GameService, RoomService } from './services';

import { Room } from './types';

const username = sessionStorage.getItem('username');

if (!username) {
  window.location.replace('/login');
}

if (username) {
  const socket = io('', { query: { username } });

  const gameService = new GameService({ socket });

  const roomService = new RoomService({ socket });

  const appService = new AppService({
    username,
    socket,
    roomService,
    gameService,
  });

  const onUsernameExists = () => {
    appService.showError('Username already exists!', () => {
      sessionStorage.removeItem('username');
      window.location.replace('/login');
    });
  };

  const onRoomExists = () => {
    appService.showError('Room with the same name already exists!');
  };

  const onUpdateRooms = (rooms: Room[]) => {
    appService.updateRooms(rooms);
  };

  const onUpdateRoom = (room: Room) => {
    appService.updateRoom(room);

    const isGameStarted = gameService.isStarted();
    const canStartGame = !isGameStarted && room.users.every(({ isReady }) => isReady);
    const isGameFinished = room.users.every(({ progress }) => progress === 100);

    if (canStartGame) {
      appService.setGame();
      gameService.prestartGame(room.name);
    }
    if (isGameStarted && isGameFinished) {
      gameService.finishGame();
    }
  };

  const onCreateRoomDone = (roomName: string) => {
    appService.joinRoom(roomName);
  };

  const onJoinRoomDone = (roomName: string) => {
    appService.setActiveRoom(roomName);
  };

  const onUpdateTimer = (value: number) => {
    gameService.updateTimer(value);
  };

  const onFinishTimer = () => {
    gameService.startGame();
  };

  const onUpdateGameTime = (value: number) => {
    gameService.updateGameTime(value);
  };

  const onFinishGameTime = () => {
    const isGameStarted = gameService.isStarted();

    if (isGameStarted) {
      gameService.finishGame();
    }
  };

  const onSetText = (textId: number) => {
    gameService.setText(textId);
  };

  const onGameResults = (results: string[]) => {
    appService.showModal(results);
  };

  socket.on('USERNAME_EXISTS', onUsernameExists);
  socket.on('ROOM_EXISTS', onRoomExists);
  socket.on('UPDATE_ROOMS', onUpdateRooms);
  socket.on('UPDATE_ROOM', onUpdateRoom);
  socket.on('CREATE_ROOM_DONE', onCreateRoomDone);
  socket.on('JOIN_ROOM_DONE', onJoinRoomDone);
  socket.on('UPDATE_TIMER', onUpdateTimer);
  socket.on('FINISH_TIMER', onFinishTimer);
  socket.on('TEXT_ID', onSetText);
  socket.on('UPDATE_GAME_TIME', onUpdateGameTime);
  socket.on('FINISH_GAME_TIME', onFinishGameTime);
  socket.on('GAME_RESULTS', onGameResults);
}
