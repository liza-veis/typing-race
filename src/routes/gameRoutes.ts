import { Router } from 'express';
import path from 'path';
import data from '../data';
import { HTML_FILES_PATH } from '../config';

const router = Router();

router.get('/', (_req, res) => {
  const page = path.join(HTML_FILES_PATH, 'game.html');
  res.sendFile(page);
});

router.get('/texts/:id', (req, res) => {
  const { id } = req.params;
  const text = data.texts[+id];
  res.send(text);
});

export default router;
