import path from 'path';
import fs from 'fs';
import commonjs from '@rollup/plugin-commonjs';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import typescript from '@rollup/plugin-typescript';
import copy from 'rollup-plugin-copy';
import { terser } from 'rollup-plugin-terser';

const srcDirPath = path.resolve(__dirname, 'src/public');
const destDirPath = path.resolve(__dirname, 'dist/public');

const srcPath = {
  scripts: path.join(srcDirPath, 'scripts'),
  tsconfig: path.join(srcDirPath, 'tsconfig.json'),
  assets: 'src/public/(html|styles)',
};

const destPath = {
  scripts: path.join(destDirPath, 'javascript'),
  assets: destDirPath,
};

const entries = fs.readdirSync(srcPath.scripts).filter(path.extname);
export default [
  ...entries.map((entry) => ({
    input: path.join(srcPath.scripts, entry),
    output: {
      file: path.join(destPath.scripts, `${path.parse(entry).name}.mjs`),
      format: 'esm',
    },
    plugins: [
      commonjs(),
      nodeResolve({ browser: true }),
      typescript({ tsconfig: srcPath.tsconfig }),
      terser(),
      copy({
        targets: [
          {
            src: srcPath.assets,
            dest: destPath.assets,
          },
        ],
        copyOnce: true,
      }),
    ],
  })),
];
